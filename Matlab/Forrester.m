function [x, f, df] = Forrester(FidelityLevel,flag)

ndim=1;

f = 0.0; % function value
df = zeros(1,ndim);	% gradient

ub=ones(1,ndim);
lb=zeros(1,ndim);

x=zeros(1,ndim);

%=== Read coordinates and transform===
variables = load('variables.inp');
x=variables(1:ndim);
if (min(x)<0.0 || max(x)>1.0)
    disp('WARNING Input not in unit interval')
end
x=reshape(x,size(ub));
x=x.*(ub-lb)+lb;


f = ((6.0*x-2.0)^2.0)*sin(12.0*x-4.0);

if(FidelityLevel==2)
    f = ((5.50*x-2.5)^2.0)*sin(12.0*x-4.0);
elseif(FidelityLevel==3)
    f = 0.75*f + 5.0*(x-0.5)-2.0;
elseif(FidelityLevel==4)
    f = 0.5*f + 10.0*(x-0.5)-5.0;
end
