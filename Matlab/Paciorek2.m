function [x, f, df] = Paciorek2(FidelityLevel,flag)

ndim=2;

f = 0.0; % function value
df = zeros(1,ndim);	% gradient

ub=1*ones(1,ndim);
lb=0.3*ones(1,ndim);

x=zeros(1,ndim);

%=== Read coordinates and transform===
variables = load('variables.inp');
x=variables(1:ndim);
if (min(x)<0.0 || max(x)>1.0)
    disp('WARNING Input not in unit interval')
end
x=reshape(x,size(ub));
x=x.*(ub-lb)+lb;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
A2 = 0.5;
Noise1 = 0.0125;
Noise2 = 0.075;
f=sin(1/(x(1)*x(2)));

if (FidelityLevel==0)
    f = f;
elseif(FidelityLevel==1)
    f = f +  normrnd(0,Noise1);
elseif(FidelityLevel==2)
    f= f - 9 * A2^2 * cos(1/(x(1)*x(2))) +  normrnd(0,Noise2);
end
