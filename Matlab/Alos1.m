function [x, f, df] = Alos1(FidelityLevel,flag)

ndim=1;

f = 0.0; % function value
df = zeros(1,ndim);	% gradient

ub=ones(1,ndim);
lb=zeros(1,ndim);

x=zeros(1,ndim);

%=== Read coordinates and transform===
variables = load('variables.inp');
x=variables(1:ndim);
if (min(x)<0.0 || max(x)>1.0)
    disp('WARNING Input not in unit interval')
end
x=reshape(x,size(ub));
x=x.*(ub-lb)+lb;

f=sin(30.0*(x-0.9)^4)*cos(2.0*(x-0.9))+(x-0.9)/2.0;

if (flag>0)
    df=120.0*(x-0.9)^3*cos(30.0*(x-0.9)^4)*cos(2.0*(x-0.9)) - 2.0*sin(30.0*(x-0.9)^4)*sin(2.0*(x-0.9)) + 0.5;
end

if(FidelityLevel==2)
    if (flag>0)
        df=( (df+1.0)*(1.0+0.25*x)-0.25*(f-1.0+x) ) / (1.0+0.25*x)^2;
    end
    
    f=(f-1.0+x)/(1.0+0.25*x);
end



