function [x, f, df] = Rastrigin2(FidelityLevel,flag)

ndim=2;

f = 0.0; % function value
df = zeros(1,ndim);	% gradient

ub=0.2*ones(1,ndim);
lb=-0.1*ones(1,ndim);

x=zeros(1,ndim);
Xtrasla=zeros(1,ndim);

xStar=0.1*ones(1,ndim);

%=== Read coordinates and transform===
variables = load('variables.inp');
x=variables(1:ndim);
if (min(x)<0.0 || max(x)>1.0)
    disp('WARNING Input not in unit interval')
end
x=reshape(x,size(ub));
x=x.*(ub-lb)+lb;
Xtrasla = x - xStar;


Theta=0.2;
Rmat = [[cos(Theta),-sin(Theta)];[sin(Theta),cos(Theta)] ];

disp(Rmat)

z=zeros(1,ndim);
for k = 1:ndim
    for j = 1:ndim
        z(k)=z(k)+Rmat(k,j)*Xtrasla(j);
    end
end
f=0.0;
for k =1:ndim
    f = f + (z(k)^2.0 + 1.0 - cos(10.0*pi*z(k)));
end

if(FidelityLevel==1)
    Phi=10000.0 ;
elseif(FidelityLevel==2)
    Phi=5000.0;
elseif(FidelityLevel==3)
    Phi=2500.0;
end
TH= 1.0-0.0001*Phi;
a = TH;
w = 10.0*pi*TH;
b = 0.5*pi*TH;

er=0.0;
for k = 1:ndim
    er = er + a*(cos(w*z(k)+b+pi)^2.0);
end
f=f+er;
