function stru = read_namelist_m(filename, namelist)

mlist=[];
fid = fopen(filename);
while isempty(mlist)
  line = fgetl(fid);
  mlist = regexp(line, "^\s*&(" + namelist + ")", 'match');
end

listend=[];
while isempty(listend)
    line = fgetl(fid);
    pp=textscan(line,'%s');
    if size(pp{1},1)>=1
        if pp{1}{1}=='/'
            break
        end
        if pp{1}{1}~='!'
            VVV = str2double(pp{1}{3});
            if isnan(VVV)
                VVV=pp{1}{3};
            end
            stru.(pp{1}{1})=VVV;
        end
    end
end
fclose(fid);