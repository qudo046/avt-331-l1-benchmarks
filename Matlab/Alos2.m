function [x, f, df] = Alos2(FidelityLevel,flag)

ndim=2;

f = 0.0; % function value
df = zeros(1,ndim);	% gradient

ub=ones(1,ndim);
lb=zeros(1,ndim);

x=zeros(1,ndim);

%=== Read coordinates and transform===
variables = load('variables.inp');
x=variables(1:ndim);
if (min(x)<0.0 || max(x)>1.0)
    disp('WARNING Input not in unit interval')
end
x=reshape(x,size(ub));
x=x.*(ub-lb)+lb;

f=sin(21.0*(x(1)-0.9)^4)*cos(2.0*(x(1)-0.9))+(x(1)-0.7)/2.0+ 2.0*x(2)^2*sin(x(1)*x(2));

if (flag>0)
    df(1)=84.0*(x(1)-0.9)^3*cos(21.0*(x(1)-0.9)^4)*cos(2.0*(x(1)-0.9)) - 2.0*sin(21.0*(x(1)-0.9)^4)*sin(2.0*(x(1)-0.9)) + 0.5 + 2.0*x(2)^3*cos(x(1)*x(2));
    df(2)=4.0*x(2)*sin(x(1)*x(2)) + 2.0*x(2)^2*x(1)*cos(x(1)*x(2));
end

if(FidelityLevel==2)
    if (flag>0)
        df(1)=( (df(1)+1.0)*(5.0+0.25*x(1)+0.5*x(2))-0.25*(f-2.0+x(1)+x(2)) ) / (5.0+0.25*x(1)+0.5*x(2))^2;
        df(2)=( (df(2)+1.0)*(5.0+0.25*x(1)+0.5*x(2))-0.5*(f-2.0+x(1)+x(2)) ) / (5.0+0.25*x(1)+0.5*x(2))^2;
    end
    
    f=(f-2.0+x(1)+x(2))/(5.0+0.25*x(1)+0.5*x(2));
end
