function main()

flag=1;   % 0: function value only, >0: function and gradient value (only implemented for Rosenbrock and ALOS functions)

nml = read_namelist_m('L1-input.nml','CaseInfo');
FidelityLevel=nml.FidelityLevel;

InfoCase=nml.InfoCase;
disp('InfoCase:')
disp(InfoCase)
disp(' ')
disp('Fidelity Level:')
disp(FidelityLevel)

if strcmp(InfoCase,'1DFORRESTER')
    [x,f,df]=Forrester(FidelityLevel,flag);
    
elseif strcmp(InfoCase,'1DALOS')
    [x,f,df]=Alos1(FidelityLevel,flag);
    
elseif strcmp(InfoCase,'2DALOS')
    [x,f,df]=Alos2(FidelityLevel,flag);
    
elseif strcmp(InfoCase,'3DALOS')
    [x,f,df]=Alos3(FidelityLevel,flag);
    
elseif strcmp(InfoCase,'2DROSENBROCK')
    [x,f,df]=Rosenbrock2(FidelityLevel,flag);
    
elseif strcmp(InfoCase,'5DROSENBROCK')
    [x,f,df]=Rosenbrock5(FidelityLevel,flag);
    
elseif strcmp(InfoCase,'10DROSENBROCK')
    [x,f,df]=Rosenbrock10(FidelityLevel,flag);
    
elseif strcmp(InfoCase,'2DRASTRIGIN')
    [x,f,df]=Rastrigin2(FidelityLevel,flag);
    
elseif strcmp(InfoCase,'5DRASTRIGIN')
    [x,f,df]=Rastrigin5(FidelityLevel,flag);
    
elseif strcmp(InfoCase,'10DRASTRIGIN')
    [x,f,df]=Rastrigin10(FidelityLevel,flag);
    
elseif strcmp(InfoCase,'2DMASS')
    [x,f,df]=Mass(FidelityLevel,flag);
    
elseif strcmp(InfoCase,'2DSPRING')
    [x,f,df]=Spring(FidelityLevel,flag);
    
elseif strcmp(InfoCase,'4DSPRINGMASS')
    [x,f,df]=SpringMass(FidelityLevel,flag);
    
elseif strcmp(InfoCase,'1DDISCFORRESTER')
    [x,f,df]=DiscForrester(FidelityLevel,flag);
    
elseif strcmp(InfoCase,'2DPACIOREK')
    [x,f,df]=Paciorek2(FidelityLevel,flag);
end

%=== Write output ===
disp(' ')
if (flag==0)
    disp('x:')
    disp(x)
    disp('f:')
    disp(f)
else
    disp('x:')
    disp(x)
    disp('f:')
    disp(f)
    disp('df:')
    disp(df)
    disp(' ')
end
Vout=[x f df];
save('Output_matlab.dat','Vout','-ascii','-double')
