This folder contains the **Matlab** version of the L1 problems.
The Matlab implementation is similar to the python one: same inputs and same outputs for all the functions.

The "main" function in the main.m file contains an example to run all the cases. Running will require the files L1-input.nml and variables.inp.

If you spot any issue/typo, please correct/integrate or send an email to edmondo.minisci@strath.ac.uk
