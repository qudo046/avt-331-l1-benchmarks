function [yf] = rk4(y0,t0,tf,h,m,k)

% Calculating number of time steps
n = floor((tf-t0)/h);

% Time-march with RK4
for i=1:n
    k1 = h * (rhs(t0, y0,m,k));
    k2 = h * (rhs((t0+h/2), (y0+k1/2),m,k));
    k3 = h * (rhs((t0+h/2), (y0+k2/2),m,k));
    k4 = h * (rhs((t0+h), (y0+k3),m,k));
    kt = (k1+2*k2+2*k3+k4)/6.0;
    
    y0 = y0 + kt;
    t0 = t0+h;
end

yf=y0(1);
