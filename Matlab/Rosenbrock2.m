function [x, f, df] =  Rosenbrock2(FidelityLevel,flag)

ndim=2;

f = 0.0; % function value
df = zeros(1,ndim);	% gradient

ub=2.0*ones(1,ndim);
lb=-2.0*ones(1,ndim);

x=zeros(1,ndim);

%=== Read coordinates and transform===
variables = load('variables.inp');
x=variables(1:ndim);
if (min(x)<0.0 || max(x)>1.0)
    disp('WARNING Input not in unit interval')
end
x=reshape(x,size(ub));
x=x.*(ub-lb)+lb;


if(FidelityLevel==1)
    
    for k =1:ndim-1
        f = f + 100.0*(x(k+1)-x(k)^2)^2 + (1.0-x(k))^2;
    end
    if (flag>0)
        df(1) = -200.0*(x(2)-x(1)^2)*2.0*x(1) - 2.0*(1-x(1));
        for k =2:ndim-1
            df(k) = 200.0*(x(k)-x(k-1)^2) - 200.0*(x(k+1)-x(k)^2)*2.0*x(k) - 2.0*(1-x(k));
        end
        df(ndim) = 200.0*(x(ndim)-x(ndim-1)^2);
    end
    
elseif(FidelityLevel==2)
    
    for k =1:ndim-1
        f = f + 50.0*(x(k+1)-x(k)^2)^2 + (-2.0-x(k))^2 - 0.5*x(k);
    end
    f = f - 0.5*x(ndim);
    
    if (flag>0)
        df(1) = -100.0*(x(2)-x(1)^2)*2.0*x(1) - 2.0*(-2.0-x(1))- 0.5;
        for k =2:ndim-1
            df(k) = 100.0*(x(k)-x(k-1)^2) - 100.0*(x(k+1)-x(k)^2)*2.0*x(k) - 2.0*(-2.0-x(k)) - 0.5;
        end
        df(ndim) = 100.0*(x(ndim)-x(ndim-1)^2) - 0.5;
    end
    
elseif(FidelityLevel==3)
    
    for k =1:ndim-1
        f = f + 100.0*(x(k+1)-x(k)^2)^2 + (1.0-x(k))^2;
    end
    if (flag>0)
        df(1) = -200.0*(x(2)-x(1)^2)*2.0*x(1) - 2.0*(1-x(1));
        for k =2:ndim-1
            df(k) = 200.0*(x(k)-x(k-1)^2) - 200.0*(x(k+1)-x(k)^2)*2.0*x(k) - 2.0*(1-x(k));
        end
        df(ndim) = 200.0*(x(ndim)-x(ndim-1)^2);
    end
    
    b0 = 4.0;
    bi = 0.5*ones(1,ndim);
    a0 = 10.0;
    ai = 0.25*ones(1,ndim); % Be careful that the denom != 0
    %subtract additive terms
    delta = b0;
    for i = 1:ndim
        delta = delta + bi(i)*x(i);
    end
    uval = f - delta;
    %divide by multiplicative terms
    fac = a0;
    for i=1:ndim
        fac = fac + ai(i)*x(i);
    end
    uval = uval / fac;
    
    if (flag>0)
        for i =1:ndim
            df(i) = ( fac*(df(i)-bi(i)) - ai(i)*(f-delta) ) / (fac^2);
        end
    end
    f = uval;
end
