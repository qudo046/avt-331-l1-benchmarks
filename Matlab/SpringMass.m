function [ x, f, df] = SpringMass(FidelityLevel,flag)

ndim=4;

f = 0.0; % function value
df = zeros(1,ndim);	% gradient

ub=4.0*ones(1,ndim);
lb=ones(1,ndim);


x=[0.0, 0.0, 0.0, 0.0];
%=== Read coordinates and transform===
variables = load('variables.inp');
x=variables(1:ndim);
if (min(x)<0.0 || max(x)>1.0)
    disp('WARNING Input not in unit interval')
end
x=reshape(x,size(ub));
x=x.*(ub-lb)+lb;



if(FidelityLevel==1)
    dt=0.01;
elseif(FidelityLevel==2)
    dt=0.6;
end

t0=0.0;
tf=6.0;
y0=[1.0,0.0,0.0,0.0];

m=x(1,3:4);
k=x(1,1:2);

f=rk4(y0,t0,tf,dt,m,k);
