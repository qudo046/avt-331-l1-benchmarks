from scipy.stats import qmc
from scipy.io import savemat
import datetime
import numpy

haltonSize = 1000000
haltonDim = 30
chunkSize = 100000 #haltonSize

writeMat = False
writeCSV = True

haltonSampler = qmc.Halton(d=haltonDim, scramble=True, seed=42)
haltonSet = haltonSampler.random(haltonSize)

#haltonSampler.reset()

#print(haltonSet[0])

if writeCSV:
    filename = "AVT331SampleSet_{}D_1M.csv".format(haltonDim)
    with open(filename,'w') as outfile:
        line = "# Quasi-random sequence from scipy.stats.qmc.Halton for computing RMSE\n"
        line = line + "# Dimension = {}, sample size = {}\n".format(haltonDim, haltonSize)
        line = line + "# Dean Bryson, Air Force Research Laboratory, Wright-Patterson AFB, OH, USA\n"
        line = line + "# {}\n".format(datetime.datetime.now())
        outfile.write(line)

        for point in haltonSet:
            line = "{:15.14E}".format(point[0])
            for coord in point[1:]:
                line = line + ", {:15.14E}".format(coord)
            line = line + "\n"
            outfile.write(line)

    filename = "AVT331SampleSet_{}D_100K.csv".format(haltonDim)
    with open(filename,'w') as outfile:
        line = "# Quasi-random sequence from scipy.stats.qmc.Halton for computing RMSE\n"
        line = line + "# Dimension = {}, sample size = {}\n".format(haltonDim, chunkSize)
        line = line + "# Dean Bryson, Air Force Research Laboratory, Wright-Patterson AFB, OH, USA\n"
        line = line + "# {}\n".format(datetime.datetime.now())
        outfile.write(line)

        for point in haltonSet[0:chunkSize]:
            line = "{:15.14E}".format(point[0])
            for coord in point[1:]:
                line = line + ", {:15.14E}".format(coord)
            line = line + "\n"
            outfile.write(line)

if writeMat:
    tmp = {}
    tmp["halton"] = haltonSet
    savemat("halton.mat", tmp)

