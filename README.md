# AVT-331 L1 BENCHMARKS

This repository presents a collection of analytical benchmark problems specifically selected to provide a set of stress
tests for the assessment of multi-fidelity optimization methods. These benchmarks, here denoted as L1 problems,
address challenges related to the curse of dimensionality and the scalability associated with multi-fidelity
methods, handling localized, multimodal, and discontinuous behaviors of the objective functions, and handling
the possible presence of noise in the objective functions. The first objective is to provide the community with
hard-to-solve but easy-to-implement problems. The second objective is to assess and compare quite a large
variety of multi-fidelity methods against benchmark problems. Multi-fidelity methods performance are assessed
in terms of both optimization and global accuracy.

## License
GNU GENERAL PUBLIC LICENSE
Version 2, June 1991
