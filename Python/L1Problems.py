import math
import f90nml
import numpy as np


def Forrester(FidelityLevel,flag):

    ndim=1
    
    f = 0.0 # function value
    df = [0.0 for x in range(ndim)]	# gradient

    ub=[1.0 for x in range(ndim)]
    lb=[0.0 for x in range(ndim)]

    x=[0.0 for x in range(ndim)]

    #=== Read coordinates and transform===
    coorfile = open('variables.inp','rt')
    for k in range(ndim):
        line = coorfile.readline()
        x[k] = float(line)
        if (x[k]<0.0 or x[k]>1.0):
            print ('WARNING: Input not in unit interval')
        x[k] = x[k]*(ub[k]-lb[k])+lb[k]
    coorfile.close()
 
    f = ((6.0*x[0]-2.0)**2.0)*math.sin(12.0*x[0]-4.0)

    if(FidelityLevel==2):
        f = ((5.50*x[0]-2.5)**2.0)*math.sin(12.0*x[0]-4.0)
    if(FidelityLevel==3):
        f = 0.75*f + 5.0*(x[0]-0.5)-2.0
    if(FidelityLevel==4):
        f = 0.5*f + 10.0*(x[0]-0.5)-5.0

    return x, f, df

def Alos1(FidelityLevel,flag):

    ndim=1
    
    f = 0.0 # function value
    df = [0.0 for x in range(ndim)]	# gradient

    ub=[1.0 for x in range(ndim)]
    lb=[0.0 for x in range(ndim)]

    x=[0.0 for x in range(ndim)]

    #=== Read coordinates and transform===
    coorfile = open('variables.inp','rt')
    for k in range(ndim):
        line = coorfile.readline()
        x[k] = float(line)
        if (x[k]<0.0 or x[k]>1.0):
            print ('WARNING: Input not in unit interval')
        x[k] = x[k]*(ub[k]-lb[k])+lb[k]
    coorfile.close()

    f=math.sin(30.0*(x[0]-0.9)**4)*math.cos(2.0*(x[0]-0.9))+(x[0]-0.9)/2.0

    if (flag>0):
        df[0]=120.0*(x[0]-0.9)**3*math.cos(30.0*(x[0]-0.9)**4)*math.cos(2.0*(x[0]-0.9)) - 2.0*math.sin(30.0*(x[0]-0.9)**4)*math.sin(2.0*(x[0]-0.9)) + 0.5

    if(FidelityLevel==2):
        if (flag>0):
            df[0]=( (df[0]+1.0)*(1.0+0.25*x[0])-0.25*(f-1.0+x[0]) ) / (1.0+0.25*x[0])**2

        f=(f-1.0+x[0])/(1.0+0.25*x[0])
 

    return x, f, df


def Alos2(FidelityLevel,flag):

    ndim=2
    
    f = 0.0 # function value
    df = [0.0 for x in range(ndim)]	# gradient

    ub=[1.0 for x in range(ndim)]
    lb=[0.0 for x in range(ndim)]

    x=[0.0 for x in range(ndim)]

    #=== Read coordinates and transform===
    coorfile = open('variables.inp','rt')
    for k in range(ndim):
        line = coorfile.readline()
        x[k] = float(line)
        if (x[k]<0.0 or x[k]>1.0):
            print ('WARNING: Input not in unit interval')
        x[k] = x[k]*(ub[k]-lb[k])+lb[k]
    coorfile.close()

    f=math.sin(21.0*(x[0]-0.9)**4)*math.cos(2.0*(x[0]-0.9))+(x[0]-0.7)/2.0+ 2.0*x[1]**2*math.sin(x[0]*x[1])

    if (flag>0):
        df[0]=84.0*(x[0]-0.9)**3*math.cos(21.0*(x[0]-0.9)**4)*math.cos(2.0*(x[0]-0.9)) - 2.0*math.sin(21.0*(x[0]-0.9)**4)*math.sin(2.0*(x[0]-0.9)) + 0.5 + 2.0*x[1]**3*math.cos(x[0]*x[1])
        df[1]=4.0*x[1]*math.sin(x[0]*x[1]) + 2.0*x[1]**2*x[0]*math.cos(x[0]*x[1])
     

    if(FidelityLevel==2):
        if (flag>0):
            df[0]=( (df[0]+1.0)*(5.0+0.25*x[0]+0.5*x[1])-0.25*(f-2.0+x[0]+x[1]) ) / (5.0+0.25*x[0]+0.5*x[1])**2
            df[1]=( (df[1]+1.0)*(5.0+0.25*x[0]+0.5*x[1])-0.5*(f-2.0+x[0]+x[1]) ) / (5.0+0.25*x[0]+0.5*x[1])**2

        f=(f-2.0+x[0]+x[1])/(5.0+0.25*x[0]+0.5*x[1])

    return x, f, df


def Alos3(FidelityLevel,flag):

    ndim=3
    
    f = 0.0 # function value
    df = [0.0 for x in range(ndim)]	# gradient

    ub=[1.0 for x in range(ndim)]
    lb=[0.0 for x in range(ndim)]

    x=[0.0 for x in range(ndim)]

    #=== Read coordinates and transform===
    coorfile = open('variables.inp','rt')
    for k in range(ndim):
        line = coorfile.readline()
        x[k] = float(line)
        if (x[k]<0.0 or x[k]>1.0):
            print ('WARNING: Input not in unit interval')
        x[k] = x[k]*(ub[k]-lb[k])+lb[k]
    coorfile.close()

    f=math.sin(21.0*(x[0]-0.9)**4)*math.cos(2.0*(x[0]-0.9))+(x[0]-0.7)/2.0+ 2.0*x[1]**2*math.sin(x[0]*x[1])+3.0*x[2]**3*math.sin(x[0]*x[1]*x[2])

    if (flag>0):
        df[0]=84.0*(x[0]-0.9)**3*math.cos(21.0*(x[0]-0.9)**4)*math.cos(2.0*(x[0]-0.9)) - 2.0*math.sin(21.0*(x[0]-0.9)**4)*math.sin(2.0*(x[0]-0.9)) + 0.5 + 2.0*x[1]**3*math.cos(x[0]*x[1]) + 3.0*x[1]*x[2]**4*math.cos(x[0]*x[1]*x[2])
        df[1]=4.0*x[1]*math.sin(x[0]*x[1]) + 2.0*x[1]**2*x[0]*math.cos(x[0]*x[1]) + 3.0*x[0]*x[2]**4*math.cos(x[0]*x[1]*x[2])
        df[2]= 9.0*x[2]**2*math.sin(x[0]*x[1]*x[2])  + 3.0*x[2]**3*x[0]*x[1]*math.cos(x[0]*x[1]*x[2])

    if(FidelityLevel==2):
        if (flag>0):      
            df[0]=( (df[0]+1.0)*(5.0+0.25*x[0]+0.5*x[1]-0.75*x[2])-0.25*(f-2.0+x[0]+x[1]+x[2]) ) / (5.0+0.25*x[0]+0.5*x[1]-0.75*x[2])**2
            df[1]=( (df[1]+1.0)*(5.0+0.25*x[0]+0.5*x[1]-0.75*x[2])-0.5*(f-2.0+x[0]+x[1]+x[2]) ) / (5.0+0.25*x[0]+0.5*x[1]-0.75*x[2])**2
            df[2]=( (df[2]+1.0)*(5.0+0.25*x[0]+0.5*x[1]-0.75*x[2])+0.75*(f-2.0+x[0]+x[1]+x[2]) ) / (5.0+0.25*x[0]+0.5*x[1]-0.75*x[2])**2

        f=(f-2.0+x[0]+x[1]+x[2])/(5.0+0.25*x[0]+0.5*x[1]-0.75*x[2])

    return x, f, df


def Rosenbrock2(FidelityLevel,flag):

    ndim=2
    
    f = 0.0 # function value
    df = [0.0 for x in range(ndim)]	# gradient

    ub=[2.0 for x in range(ndim)]
    lb=[-2.0 for x in range(ndim)]

    x=[0.0 for x in range(ndim)]

    #=== Read coordinates and transform===
    coorfile = open('variables.inp','rt')
    for k in range(ndim):
        line = coorfile.readline()
        x[k] = float(line)
        if (x[k]<0.0 or x[k]>1.0):
            print ('WARNING: Input not in unit interval')
        x[k] = x[k]*(ub[k]-lb[k])+lb[k]
    coorfile.close()

    if(FidelityLevel==1):
    
        for k in range(ndim-1):
            f = f + 100.0*(x[k+1]-x[k]**2)**2 + (1.0-x[k])**2

        if (flag>0):
            df[0] = -200.0*(x[1]-x[0]**2)*2.0*x[0] - 2.0*(1-x[0])
            for k in range(1,ndim-1):
                df[k] = 200.0*(x[k]-x[k-1]**2) - 200.0*(x[k+1]-x[k]**2)*2.0*x[k] - 2.0*(1-x[k])
            df[ndim-1] = 200.0*(x[ndim-1]-x[ndim-2]**2)


    if(FidelityLevel==2):

        for k in range(ndim-1):
            f = f + 50.0*(x[k+1]-x[k]**2)**2 + (-2.0-x[k])**2 - 0.5*x[k]
        f = f - 0.5*x[ndim-1]

        if (flag>0):
            df[0] = -100.0*(x[1]-x[0]**2)*2.0*x[0] - 2.0*(-2.0-x[0])- 0.5
            for k in range(1,ndim-1):
                df[k] = 100.0*(x[k]-x[k-1]**2) - 100.0*(x[k+1]-x[k]**2)*2.0*x[k] - 2.0*(-2.0-x[k]) - 0.5
            df[ndim-1] = 100.0*(x[ndim-1]-x[ndim-2]**2) - 0.5

    if(FidelityLevel==3):
    
        for k in range(ndim-1):
            f = f + 100.0*(x[k+1]-x[k]**2)**2 + (1.0-x[k])**2

        if (flag>0):
            df[0] = -200.0*(x[1]-x[0]**2)*2.0*x[0] - 2.0*(1-x[0])
            for k in range(1,ndim-1):
                df[k] = 200.0*(x[k]-x[k-1]**2) - 200.0*(x[k+1]-x[k]**2)*2.0*x[k] - 2.0*(1-x[k])
            df[ndim-1] = 200.0*(x[ndim-1]-x[ndim-2]**2)


        b0 = 4.0
        bi = [0.5 for x in range(ndim)]
        a0 = 10.0
        ai = [0.25 for x in range(ndim)] # Be careful that the denom != 0
        #subtract additive terms
        delta = b0
        for i in range(ndim):
            delta = delta + bi[i]*x[i]
        uval = f - delta
        #divide by multiplicative terms
        fac = a0
        for i in range(ndim):
            fac = fac + ai[i]*x[i]
        uval = uval / fac

        if (flag>0):
            for i in range(ndim):
                df[i] = ( fac*(df[i]-bi[i]) - ai[i]*(f-delta) ) / (fac**2)

        f = uval
              
        

    return x, f, df


def Rosenbrock5(FidelityLevel,flag):

    ndim=5
    
    f = 0.0 # function value
    df = [0.0 for x in range(ndim)]	# gradient

    ub=[2.0 for x in range(ndim)]
    lb=[-2.0 for x in range(ndim)]

    x=[0.0 for x in range(ndim)]

    #=== Read coordinates and transform===
    coorfile = open('variables.inp','rt')
    for k in range(ndim):
        line = coorfile.readline()
        x[k] = float(line)
        if (x[k]<0.0 or x[k]>1.0):
            print ('WARNING: Input not in unit interval')
        x[k] = x[k]*(ub[k]-lb[k])+lb[k]
    coorfile.close()

    if(FidelityLevel==1):
    
        for k in range(ndim-1):
            f = f + 100.0*(x[k+1]-x[k]**2)**2 + (1.0-x[k])**2

        if (flag>0):
            df[0] = -200.0*(x[1]-x[0]**2)*2.0*x[0] - 2.0*(1-x[0])
            for k in range(1,ndim-1):
                df[k] = 200.0*(x[k]-x[k-1]**2) - 200.0*(x[k+1]-x[k]**2)*2.0*x[k] - 2.0*(1-x[k])
            df[ndim-1] = 200.0*(x[ndim-1]-x[ndim-2]**2)


    if(FidelityLevel==2):

        for k in range(ndim-1):
            f = f + 50.0*(x[k+1]-x[k]**2)**2 + (-2.0-x[k])**2 - 0.5*x[k]
        f = f - 0.5*x[ndim-1]

        if (flag>0):
            df[0] = -100.0*(x[1]-x[0]**2)*2.0*x[0] - 2.0*(-2.0-x[0])- 0.5
            for k in range(1,ndim-1):
                df[k] = 100.0*(x[k]-x[k-1]**2) - 100.0*(x[k+1]-x[k]**2)*2.0*x[k] - 2.0*(-2.0-x[k]) - 0.5
            df[ndim-1] = 100.0*(x[ndim-1]-x[ndim-2]**2) - 0.5

    if(FidelityLevel==3):
    
        for k in range(ndim-1):
            f = f + 100.0*(x[k+1]-x[k]**2)**2 + (1.0-x[k])**2

        if (flag>0):
            df[0] = -200.0*(x[1]-x[0]**2)*2.0*x[0] - 2.0*(1-x[0])
            for k in range(1,ndim-1):
                df[k] = 200.0*(x[k]-x[k-1]**2) - 200.0*(x[k+1]-x[k]**2)*2.0*x[k] - 2.0*(1-x[k])
            df[ndim-1] = 200.0*(x[ndim-1]-x[ndim-2]**2)


        b0 = 4.0
        bi = [0.5 for x in range(ndim)]
        a0 = 10.0
        ai = [0.25 for x in range(ndim)] # Be careful that the denom != 0
        #subtract additive terms
        delta = b0
        for i in range(ndim):
            delta = delta + bi[i]*x[i]
        uval = f - delta
        #divide by multiplicative terms
        fac = a0
        for i in range(ndim):
            fac = fac + ai[i]*x[i]
        uval = uval / fac

        if (flag>0):
            for i in range(ndim):
                df[i] = ( fac*(df[i]-bi[i]) - ai[i]*(f-delta) ) / (fac**2)

        f = uval
              
        

    return x, f, df


def Rosenbrock10(FidelityLevel,flag):

    ndim=10
    
    f = 0.0 # function value
    df = [0.0 for x in range(ndim)]	# gradient

    ub=[2.0 for x in range(ndim)]
    lb=[-2.0 for x in range(ndim)]

    x=[0.0 for x in range(ndim)]

    #=== Read coordinates and transform===
    coorfile = open('variables.inp','rt')
    for k in range(ndim):
        line = coorfile.readline()
        x[k] = float(line)
        if (x[k]<0.0 or x[k]>1.0):
            print ('WARNING: Input not in unit interval')
        x[k] = x[k]*(ub[k]-lb[k])+lb[k]
    coorfile.close()

    if(FidelityLevel==1):
    
        for k in range(ndim-1):
            f = f + 100.0*(x[k+1]-x[k]**2)**2 + (1.0-x[k])**2

        if (flag>0):
            df[0] = -200.0*(x[1]-x[0]**2)*2.0*x[0] - 2.0*(1-x[0])
            for k in range(1,ndim-1):
                df[k] = 200.0*(x[k]-x[k-1]**2) - 200.0*(x[k+1]-x[k]**2)*2.0*x[k] - 2.0*(1-x[k])
            df[ndim-1] = 200.0*(x[ndim-1]-x[ndim-2]**2)


    if(FidelityLevel==2):

        for k in range(ndim-1):
            f = f + 50.0*(x[k+1]-x[k]**2)**2 + (-2.0-x[k])**2 - 0.5*x[k]
        f = f - 0.5*x[ndim-1]

        if (flag>0):
            df[0] = -100.0*(x[1]-x[0]**2)*2.0*x[0] - 2.0*(-2.0-x[0])- 0.5
            for k in range(1,ndim-1):
                df[k] = 100.0*(x[k]-x[k-1]**2) - 100.0*(x[k+1]-x[k]**2)*2.0*x[k] - 2.0*(-2.0-x[k]) - 0.5
            df[ndim-1] = 100.0*(x[ndim-1]-x[ndim-2]**2) - 0.5

    if(FidelityLevel==3):
    
        for k in range(ndim-1):
            f = f + 100.0*(x[k+1]-x[k]**2)**2 + (1.0-x[k])**2

        if (flag>0):
            df[0] = -200.0*(x[1]-x[0]**2)*2.0*x[0] - 2.0*(1-x[0])
            for k in range(1,ndim-1):
                df[k] = 200.0*(x[k]-x[k-1]**2) - 200.0*(x[k+1]-x[k]**2)*2.0*x[k] - 2.0*(1-x[k])
            df[ndim-1] = 200.0*(x[ndim-1]-x[ndim-2]**2)


        b0 = 4.0
        bi = [0.5 for x in range(ndim)]
        a0 = 10.0
        ai = [0.25 for x in range(ndim)] # Be careful that the denom != 0
        #subtract additive terms
        delta = b0
        for i in range(ndim):
            delta = delta + bi[i]*x[i]
        uval = f - delta
        #divide by multiplicative terms
        fac = a0
        for i in range(ndim):
            fac = fac + ai[i]*x[i]
        uval = uval / fac

        if (flag>0):
            for i in range(ndim):
                df[i] = ( fac*(df[i]-bi[i]) - ai[i]*(f-delta) ) / (fac**2)

        f = uval
              
        

    return x, f, df


def Rastrigin2(FidelityLevel,flag):

    ndim=2
    
    f = 0.0 # function value
    df = [0.0 for x in range(ndim)]	# gradient

    ub=[0.2 for x in range(ndim)]
    lb=[-0.1 for x in range(ndim)]

    x=[0.0 for x in range(ndim)]
    Xtrasla=[0.0 for x in range(ndim)]
    
    xStar=[0.1 for x in range(ndim)]

    #=== Read coordinates and transform===
    coorfile = open('variables.inp','rt')
    for k in range(ndim):
        line = coorfile.readline()
        x[k] = float(line)
        if (x[k]<0.0 or x[k]>1.0):
            print ('WARNING: Input not in unit interval')
        x[k] = x[k]*(ub[k]-lb[k])+lb[k]
        Xtrasla[k] = x[k] - xStar[k]
        
    coorfile.close()

    Theta=0.2
    Rmat = [[math.cos(Theta),-math.sin(Theta)],[math.sin(Theta),math.cos(Theta)] ]

    print(Rmat)
    
    z=[0.0 for x in range(ndim)]
    for k in range(ndim):
        for j in range(ndim):
            z[k]=z[k]+Rmat[k][j]*Xtrasla[j]

    f=0.0
    for k in range(ndim):
        f = f + (z[k]**2.0 + 1.0 - math.cos(10.0*math.pi*z[k]))
        

    if(FidelityLevel==1):
        Phi=10000.0 
    if(FidelityLevel==2):
        Phi=5000.0
    if(FidelityLevel==3):
        Phi=2500.0     

    TH= 1.0-0.0001*Phi
    a = TH
    w = 10.0*math.pi*TH
    b = 0.5*math.pi*TH

    er=0.0
    for k in range(ndim):
       er = er + a*(math.cos(w*z[k]+b+math.pi)**2.0) 
      
    f=f+er
        

    return x, f, df


def Rastrigin5(FidelityLevel,flag):

    ndim=5
    
    f = 0.0 # function value
    df = [0.0 for x in range(ndim)]	# gradient

    ub=[0.2 for x in range(ndim)]
    lb=[-0.1 for x in range(ndim)]

    x=[0.0 for x in range(ndim)]
    Xtrasla=[0.0 for x in range(ndim)]
    
    xStar=[0.1 for x in range(ndim)]

    #=== Read coordinates and transform===
    coorfile = open('variables.inp','rt')
    for k in range(ndim):
        line = coorfile.readline()
        x[k] = float(line)
        if (x[k]<0.0 or x[k]>1.0):
            print ('WARNING: Input not in unit interval')
        x[k] = x[k]*(ub[k]-lb[k])+lb[k]
        Xtrasla[k] = x[k] - xStar[k]
        
    coorfile.close()

    Theta=0.2
    Rmat = [[1.0,0.0,0.0,0.0,0.0],[0.0,1.0,0.0,0.0,0.0],[0.0,0.0,1.0,0.0,0.0],[0.0,0.0,0.0,math.cos(Theta),-math.sin(Theta)],[0.0,0.0,0.0,math.sin(Theta),math.cos(Theta)]]
   
    
    z=[0.0 for x in range(ndim)]
    for k in range(ndim):
        for j in range(ndim):
            z[k]=z[k]+Rmat[k][j]*Xtrasla[j]

    f=0.0
    for k in range(ndim):
        f = f + (z[k]**2.0 + 1.0 - math.cos(10.0*math.pi*z[k]))
        

    if(FidelityLevel==1):
        Phi=10000.0 
    if(FidelityLevel==2):
        Phi=5000.0
    if(FidelityLevel==3):
        Phi=2500.0     

    TH= 1.0-0.0001*Phi
    a = TH
    w = 10.0*math.pi*TH
    b = 0.5*math.pi*TH

    er=0.0
    for k in range(ndim):
       er = er + a*(math.cos(w*z[k]+b+math.pi)**2.0) 
      
    f=f+er
        

    return x, f, df


def Rastrigin10(FidelityLevel,flag):

    ndim=10
    
    f = 0.0 # function value
    df = [0.0 for x in range(ndim)]	# gradient

    ub=[0.2 for x in range(ndim)]
    lb=[-0.1 for x in range(ndim)]

    x=[0.0 for x in range(ndim)]
    Xtrasla=[0.0 for x in range(ndim)]
    
    xStar=[0.1 for x in range(ndim)]

    #=== Read coordinates and transform===
    coorfile = open('variables.inp','rt')
    for k in range(ndim):
        line = coorfile.readline()
        x[k] = float(line)
        if (x[k]<0.0 or x[k]>1.0):
            print ('WARNING: Input not in unit interval')
        x[k] = x[k]*(ub[k]-lb[k])+lb[k]
        Xtrasla[k] = x[k] - xStar[k]
        
    coorfile.close()

    Theta=0.2
    Rmat = [[1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],[0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],[0.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],[0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0],[0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0],[0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0],[0.0,0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0],[0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0],[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,math.cos(Theta),-math.sin(Theta)],[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,math.sin(Theta),math.cos(Theta)]]
   
    
    z=[0.0 for x in range(ndim)]
    for k in range(ndim):
        for j in range(ndim):
            z[k]=z[k]+Rmat[k][j]*Xtrasla[j]

    f=0.0
    for k in range(ndim):
        f = f + (z[k]**2.0 + 1.0 - math.cos(10.0*math.pi*z[k]))
        

    if(FidelityLevel==1):
        Phi=10000.0 
    if(FidelityLevel==2):
        Phi=5000.0
    if(FidelityLevel==3):
        Phi=2500.0     

    TH= 1.0-0.0001*Phi
    a = TH
    w = 10.0*math.pi*TH
    b = 0.5*math.pi*TH

    er=0.0
    for k in range(ndim):
       er = er + a*(math.cos(w*z[k]+b+math.pi)**2.0) 
      
    f=f+er
        

    return x, f, df



def Mass(FidelityLevel,flag):

    ndim=2
    
    f = 0.0 # function value
    df = [0.0 for x in range(ndim)]	# gradient

    ub=[4.0 for x in range(ndim)]
    lb=[1.0 for x in range(ndim)]

    k=np.array([1.0, 1.0])

    
    #=== Read coordinates and transform===
    x=np.array([0.0, 0.0])
    coorfile = open('variables.inp','rt')
    for j in range(ndim):
        line = coorfile.readline()
        x[j] = float(line)
        if (x[j]<0.0 or x[j]>1.0):
            print ('WARNING: Input not in unit interval')
        x[j] = x[j]*(ub[j]-lb[j])+lb[j]
    coorfile.close()

    if(FidelityLevel==1):
        dt=0.01
    if(FidelityLevel==2):
        dt=0.6

    t0=0.0
    tf=6.0
    y0=np.array([1.0,0.0,0.0,0.0])

    f=rk4(y0,t0,tf,dt,x,k)
   
    return x, f, df


def Spring(FidelityLevel,flag):

    ndim=2
    
    f = 0.0 # function value
    df = [0.0 for x in range(ndim)]	# gradient

    ub=[4.0 for x in range(ndim)]
    lb=[1.0 for x in range(ndim)]

    m=np.array([1.0, 1.0])

    
    #=== Read coordinates and transform===
    x=np.array([0.0, 0.0])
    coorfile = open('variables.inp','rt')
    for j in range(ndim):
        line = coorfile.readline()
        x[j] = float(line)
        if (x[j]<0.0 or x[j]>1.0):
            print ('WARNING: Input not in unit interval')
        x[j] = x[j]*(ub[j]-lb[j])+lb[j]
    coorfile.close()

    if(FidelityLevel==1):
        dt=0.01
    if(FidelityLevel==2):
        dt=0.6

    t0=0.0
    tf=6.0
    y0=np.array([1.0,0.0,0.0,0.0])

    f=rk4(y0,t0,tf,dt,m,x)
   
    return x, f, df


def SpringMass(FidelityLevel,flag):

    ndim=4
    
    f = 0.0 # function value
    df = [0.0 for x in range(ndim)]	# gradient

    ub=[4.0 for x in range(ndim)]
    lb=[1.0 for x in range(ndim)]

    #=== Read coordinates and transform===
    x=np.array([0.0, 0.0, 0.0, 0.0])
    coorfile = open('variables.inp','rt')
    for j in range(ndim):
        line = coorfile.readline()
        x[j] = float(line)
        if (x[j]<0.0 or x[j]>1.0):
            print ('WARNING: Input not in unit interval')
        x[j] = x[j]*(ub[j]-lb[j])+lb[j]
    coorfile.close()

    if(FidelityLevel==1):
        dt=0.01
    if(FidelityLevel==2):
        dt=0.6

    t0=0.0
    tf=6.0
    y0=np.array([1.0,0.0,0.0,0.0])

    m=x[2:4]
    k=x[0:2]

    f=rk4(y0,t0,tf,dt,m,k)
   
    return x, f, df


# rhs for first-order system of ODEs in spring-mass system
def f(t,y,m,k):

    f=np.array([0.0,0.0,0.0,0.0])
    
    f[0]=y[2]
    f[1]=y[3]
    f[2]=(-k[0]-k[1])/m[0]*y[0] + k[1]/m[0]*y[1]
    f[3]=k[1]/m[1]*y[0] + (-k[0]-k[1])/m[1]*y[1] 
    
    return f

# RK-4 method
def rk4(y0,t0,tf,h,m,k):
    
    # Calculating number of time steps
    n = int((tf-t0)/h)

    # Time-march with RK4
    for i in range(n):
        k1 = h * (f(t0, y0,m,k))
        k2 = h * (f((t0+h/2), (y0+k1/2),m,k))
        k3 = h * (f((t0+h/2), (y0+k2/2),m,k))
        k4 = h * (f((t0+h), (y0+k3),m,k))
        kt = (k1+2*k2+2*k3+k4)/6.0
        
        y0 = y0 + kt
        t0 = t0+h

    return y0[0]


def DiscForrester(FidelityLevel,flag):

    ndim=1
    
    f = 0.0 # function value
    df = [0.0 for x in range(ndim)]	# gradient

    ub=[1.0 for x in range(ndim)]
    lb=[0.0 for x in range(ndim)]

    x=[0.0 for x in range(ndim)]

    #=== Read coordinates and transform===
    coorfile = open('variables.inp','rt')
    for k in range(ndim):
        line = coorfile.readline()
        x[k] = float(line)
        if (x[k]<0.0 or x[k]>1.0):
            print ('WARNING: Input not in unit interval')
        x[k] = x[k]*(ub[k]-lb[k])+lb[k]
    coorfile.close()
 
    if x[0]<=0.5:
        f = ((6.0*x[0]-2.0)**2.0)*math.sin(12.0*x[0]-4.0)
    elif x[0]>0.5:
        f = 10 + ((6.0*x[0]-2.0)**2.0)*math.sin(12.0*x[0]-4.0)   

    if(FidelityLevel==2):
        if x[0]<=0.5:
            f = 0.5*f + 10.0*(x[0]-0.5)-5.0
        elif x[0]>0.5:
            f = 0.5*f + 10.0*(x[0]-0.5)-7.0

    return x, f, df
    
def Paciorek2(FidelityLevel,flag):

    ndim=2
    
    f = 0.0 # function value
    df = [0.0 for x in range(ndim)]	# gradient

    ub=[1.0 for x in range(ndim)]
    lb=[0.3 for x in range(ndim)]
    
    #=== Read coordinates and transform===
    x=np.array([0.0, 0.0])
    coorfile = open('variables.inp','rt')
    for j in range(ndim):
        line = coorfile.readline()
        x[j] = float(line)
        if (x[j]<0.0 or x[j]>1.0):
            print ('WARNING: Input not in unit interval')
        x[j] = x[j]*(ub[j]-lb[j])+lb[j]
    coorfile.close()

    #########################
    A2  = 0.5
    Noise1 = 0.0125
    Noise2 = 0.075
    
    f = math.sin(1/(x[0]*x[1]))
    
    if(FidelityLevel==0):
        f =f 
    elif(FidelityLevel==1):
        f = f + np.random.normal(0, Noise1)
    elif(FidelityLevel==2):
        f = f - 9.0 * A2**2 * math.cos(1/(x[0]*x[1])) + np.random.normal(0, Noise2)
        
    return x, f, df
    
def main():

    flag=0   # 0: function value only, >0: function and gradient value (only implemented for Rosenbrock and ALOS functions)

    nml = f90nml.read('L1-input.nml')
    
    FidelityLevel=nml['caseinfo']['fidelitylevel']
    InfoCase=nml['caseinfo']['infocase']
  
    if(InfoCase[0:11]=='1DFORRESTER' or InfoCase[0:11] =='1dforrester'):
        [x,f,df]=Forrester(FidelityLevel,flag)

    if(InfoCase[0:6]=='1DALOS' or InfoCase[0:6]=='1dalos'):
        [x,f,df]=Alos1(FidelityLevel,flag)

    if(InfoCase[0:6]=='2DALOS' or InfoCase[0:6]=='2dalos'):
        [x,f,df]=Alos2(FidelityLevel,flag)

    if(InfoCase[0:6]=='3DALOS' or InfoCase[0:6]=='3dalos'):
        [x,f,df]=Alos3(FidelityLevel,flag)

    if(InfoCase[0:12]=='2DROSENBROCK' or InfoCase[0:12]=='2drosenbrock'):
        [x,f,df]=Rosenbrock2(FidelityLevel,flag)

    if(InfoCase[0:12]=='5DROSENBROCK' or InfoCase[0:12]=='5drosenbrock'):
        [x,f,df]=Rosenbrock5(FidelityLevel,flag)

    if(InfoCase[0:13]=='10DROSENBROCK' or InfoCase[0:13]=='10drosenbrock'):
        [x,f,df]=Rosenbrock10(FidelityLevel,flag)

    if(InfoCase[0:11]=='2DRASTRIGIN' or InfoCase[0:11]=='2drastrigin'):
        [x,f,df]=Rastrigin2(FidelityLevel,flag)

    if(InfoCase[0:11]=='5DRASTRIGIN' or InfoCase[0:11]=='5drastrigin'):
        [x,f,df]=Rastrigin5(FidelityLevel,flag)

    if(InfoCase[0:12]=='10DRASTRIGIN' or InfoCase[0:12]=='10drastrigin'):
        [x,f,df]=Rastrigin10(FidelityLevel,flag)

    if(InfoCase[0:6]=='2DMASS' or InfoCase[0:6]=='2dmass'):
        [x,f,df]=Mass(FidelityLevel,flag)

    if(InfoCase[0:8]=='2DSPRING' or InfoCase[0:8]=='2dspring'):
        [x,f,df]=Spring(FidelityLevel,flag)

    if(InfoCase[0:12]=='4DSPRINGMASS' or InfoCase[0:12]=='4dspringmass'):
        [x,f,df]=SpringMass(FidelityLevel,flag)

    if(InfoCase[0:15]=='1DDISCFORRESTER' or InfoCase[0:15] =='1ddiscforrester'):
        [x,f,df]=DiscForrester(FidelityLevel,flag)
    
    if(InfoCase[0:10]=='2DPACIOREK' or InfoCase[0:10]=='2dpaciorek'):
        [x,f,df]=Paciorek2(FidelityLevel,flag)

    #=== Write output ===
    print()
    if (flag==0):
        print (x,f)
    else:
        print (x,f,df)
    print()
    
    outfile = open('Output.dat','w')
    outfile.write(str(f))
    outfile.close()

main()
