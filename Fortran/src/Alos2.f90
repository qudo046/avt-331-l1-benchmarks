Subroutine Alos2(FidelityLevel,y)
implicit none
integer, parameter     :: ndv = 2
integer                :: FidelityLevel, i
real*8                 :: y, f 
real*8, dimension(ndv) :: x, ub, lb 

 ub(1:ndv) = 1.d0
 lb(1:ndv) = 0.d0
 x(:) = 0.d0
 open(15, file='variables.inp', status='old', action='read')
 do i = 1,ndv
 	read(15,*) x(i)
	 x(i) = x(i)*(ub(i)-lb(i))+lb(i)
 enddo
 close(15)

 y = 0.d0	
 f =  sin(21.d0*( (x(1)-0.9d0)**4.d0 ))*cos(2.d0*(x(1)-0.9d0)) + &
   & + (x(1)-0.7d0)/2.d0 + 2.d0*(x(2)**2.d0)*sin(x(1)*x(2))
 if(FidelityLevel.eq. 1) y = f
 if(FidelityLevel.eq. 2) y = (f - 2.d0+x(1)+x(2))/(5.d0+0.25d0*x(1)+0.5d0*x(2))
	
End Subroutine

