Subroutine rotmnd(n,v,theta,R)
! Subroutine for a general n-Dimensional Rotations with the Aguilera-Perez Algortithm
implicit none
integer n
integer c,rr,i,j
integer nrhs, info, lda, ldb, ipiv(n)
real*8 t, M(n,n), M1(n,n),v(n,n-1), v1(n,n-1), R(n,n), theta
real*8 B(n,n), A(n,n)


 do c = 1,n-2
 	do rr = n,c+1,-1
		t = atan2(v(rr,c),v(rr-1,c))
                R(:,:) = 0.d0
		do i = 1,n
			do j=1,n
				if(i.eq. j) R(i,j) = 1.d0
			enddo
		enddo
		M = R
		M1(:,:) = 0.d0
		v1(:,:) = 0.d0
		R(rr,rr) = cos(t)
		R(rr,rr-1) = sin(t)
		R(rr-1,rr) = -sin(t)
		R(rr-1,rr-1) = cos(t)

		call dgemm( 'N', 'N', n, 1, n, 1.0d0, R, n, v, n, 0.0d0, v1, n)	
		v = v1
	        call dgemm( 'N', 'N', n, n, n, 1.0d0, R, n, M, n, 0.0d0, M1, n)
		M = M1
	enddo
 enddo

 R(:,:) = 0.d0
 do i = 1,n
 	do j = 1,n
		if(i.eq. j) R(i,j) = 1.d0
	enddo
 enddo
 R(n,n) = cos(theta)
 R(n,n-1) = sin(theta)
 R(n-1,n) = -sin(theta)
 R(n-1,n-1) = cos(theta)

 call dgemm( 'N', 'N', n, n, n, 1.0d0, R, n, M, n, 0.0d0, B, n)
 nrhs = n
 lda = max(1, n)
 ldb = max(1, n)
 call dgesv( n, nrhs, M, lda, ipiv, B, ldb, info )

End Subroutine
