Subroutine RungeKutta4order(n, K, M, x0, x0dot, T0, TF, DeltaT, yT, Time)
! n = number of mass of the system mass-spring
! K = Stifness Matrix
! M = Mass Matrix
! x0 = initial condition position 
! x0dot = initial conditoin velocity
! T0 = initial time
! TF = final time
! DeltaT = Time Step
! yT = solution
implicit none
integer n, nStep
integer i, lwork, info
integer, allocatable, dimension(:) :: ipiv
real*8 DeltaT, T0, TF
real*8 Time, yT(2*n)
real*8, dimension(n) :: x0, x0dot
real*8, dimension(2*n) :: y0, K1, K2, K3, K4, TimeStep
real*8, dimension(n,n) :: K, M, eye, zero
real*8, dimension(2*n,2*n) :: A0, A1, A, InvA1
real*8, allocatable, dimension(:) :: work

! CONVERT SECON ORDER ODE Mx'' = Kx IN FIRST ORDER ODE y' = Ay

 nStep = int((TF-T0)/DeltaT) 

 eye(:,:) = 0.d0
 zero(:,:) = 0.d0
 A(:,:) = 0.d0
 do i = 1,n
    eye(i,i) = 1.d0
 enddo       

 A0(1:n,1:n) = -K(1:n,1:n)
 A0(1:n,n+1:2*n) = zero(1:n,1:n)
 A0(n+1:2*n,1:n) = zero(1:n,1:n)
 A0(n+1:2*n,n+1:2*n) = eye(1:n,1:n)

 A1(1:n,1:n) = zero(1:n,1:n)
 A1(1:n,n+1:2*n) = M(1:n,1:n)
 A1(n+1:2*n,1:n) = -eye(1:n,1:n)
 A1(n+1:2*n,n+1:2*n) = zero(1:n,1:n)

 InvA1 = A1
 lwork = 2*n
 allocate(work(lwork))
 allocate(ipiv(2*n))		

 call dgetrf( 2*n, 2*n, InvA1, 2*n, ipiv, info )
 if(info.ne. 0) then
 	write(*,*) 'Matrix is numerically singular'
 	stop
 endif	
 info = 0
 call dgetri( 2*n, InvA1, 2*n, ipiv, work, lwork, info )
 if(info.ne. 0) then
 	write(*,*) 'Matrix is numerically singular'
 	stop
 endif	
 deallocate(work)
 deallocate(ipiv)

 InvA1 = -InvA1
 call dgemm( 'N', 'N', 2*n, 2*n, 2*n, 1.0d0, InvA1, 2*n, A0, 2*n, 0.0d0, A, 2*n)  

 y0(1:n) = x0(1:n)
 y0(n+1:2*n) = x0dot(1:n)

 ! Fourth order RungeKutta
 Time = T0
 yT = y0;
 TimeStep = y0

 do i = 1,nStep

	call dgemm( 'N', 'N', 2*n, 1, 2*n, 1.0d0, A, 2*n, &
                  & TimeStep, 2*n, 0.0d0, K1, 2*n) ! K1
  
	call dgemm( 'N', 'N', 2*n, 1, 2*n, 1.0d0, A, 2*n, &
                  & (TimeStep + 0.5d0*K1*DeltaT), 2*n, 0.0d0, K2, 2*n) ! K2

	call dgemm( 'N', 'N', 2*n, 1, 2*n, 1.0d0, A, 2*n, & 
                  & (TimeStep + 0.5d0*K2*DeltaT), 2*n, 0.0d0, K3, 2*n) ! K3

	call dgemm( 'N', 'N', 2*n, 1, 2*n, 1.0d0, A, 2*n, & 
                  & (TimeStep + K3*DeltaT), 2*n, 0.0d0, K4, 2*n) ! K4

	yT(1:2*n)= TimeStep+(1.d0/6.d0)*(K1 + 2.d0*K2 + 2.d0*K3 + K4)*DeltaT
 
	TimeStep(1:2*n) = yT(1:2*n)  

	Time = i*DeltaT!Time + DeltaT	

	K1 = 0.d0
	K2 = 0.d0
	K3 = 0.d0
	K4 = 0.d0
 enddo
 
End Subroutine
