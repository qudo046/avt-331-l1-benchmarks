Subroutine Spring(FidelityLevel,y)
implicit none

integer, parameter                  :: ndv = 2, nMass = 2
real*8, parameter                   :: m1 = 1.d0, m2 = 1.d0
real*8, parameter                   :: T0 = 0.d0, Tend = 6.d0
real*8, parameter                   :: DeltaHF = 0.01d0, DeltaLF = 0.6d0
integer                             :: FidelityLevel, nStep, i
real*8                              :: y, Time
real*8, dimension(ndv)              :: x, ub, lb 
real*8, dimension(nMass)            :: x0, x0dot
real*8, dimension(nMass,nMass)      :: MassMatrix, StifnessMatrix

 ub(1:ndv) = 4.d0
 lb(1:ndv) = 1.d0
 x(:) = 0.d0
 open(15, file='variables.inp', status='old', action='read')
 do i = 1,ndv
 	read(15,*) x(i)
	 x(i) = x(i)*(ub(i)-lb(i))+lb(i)
 enddo
 close(15)

 y = 0.d0

 x0 = (/1.d0, 0.d0/)
 x0dot = (/0.d0, 0.d0/)
	
 MassMatrix(:,:) = 0.d0
 StifnessMatrix(:,:) = 0.d0

 StifnessMatrix(1,1) = -x(1)-x(2)
 StifnessMatrix(2,2) = -x(2)-x(1)
 StifnessMatrix(1,2) = x(2)
 StifnessMatrix(2,1) = x(2)
	
 MassMatrix(1,1) = m1
 MassMatrix(2,2) = m2

 if(FidelityLevel.eq. 1) then

	call RungeKutta4order(nMass, StifnessMatrix, MassMatrix, &
                             & x0, x0dot, T0, Tend, DeltaHF, y, Time)	

 elseif(FidelityLevel.eq. 2) then

	call RungeKutta4order(nMass, StifnessMatrix, MassMatrix, &
                             & x0, x0dot, T0, Tend, DeltaLF, y, Time)	

 endif

End Subroutine
