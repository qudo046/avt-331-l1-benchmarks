Subroutine Rosenbrock5(FidelityLevel,y)
implicit none
integer, parameter     :: ndv = 5
integer                :: FidelityLevel,i
real*8                 :: f2sum1,f2sum2,f3sum1,f3sum2, f1,f2,f3
real*8                 :: y  
real*8, dimension(ndv) :: x, ub, lb

 ub(1:ndv) = 2.d0
 lb(1:ndv) = -2.d0
 x(:) = 0.d0
 open(15, file='variables.inp', status='old', action='read')
 do i = 1,ndv
 	read(15,*) x(i)
	 x(i) = x(i)*(ub(i)-lb(i))+lb(i)
 enddo
 close(15)

 y = 0.d0
 f1 = 0.d0
 f2sum1 = 0.d0
 f2sum2 = 0.d0
 f3sum1 = 0.d0
 f3sum2 = 0.d0
 do i = 1,ndv
	if(i.lt. ndv) then
		f1 = f1 + 100.d0*((x(i+1)-x(i)**2.d0)**2.d0) + (1.d0-x(i))**2.d0
		f2sum1 = f2sum1 + 50.d0*((x(i+1)-x(i)**2.d0)**2.d0) + &
	               & (-2.d0-x(i))**2.d0
	endif
	f2sum2 = f2sum2 + 0.5d0*x(i)
	f3sum1 = f3sum1 + 0.5d0*x(i)
	f3sum2 = f3sum2 + 0.25d0*x(i) 
 enddo
 f2 = f2sum1- f2sum2
 f3 = (f1 - 4.d0 - f3sum1)/(10.d0+f3sum2)		
 if(FidelityLevel.eq. 1) y = f1
 if(FidelityLevel.eq. 2) y = f2
 if(FidelityLevel.eq. 3) y = f3

End Subroutine
