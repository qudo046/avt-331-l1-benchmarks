Subroutine Rastrigin10(FidelityLevel,y)
implicit none
integer, parameter      :: ndv = 10
real*8, parameter       :: pi = acos(-1.d0)
integer                 :: FidelityLevel, i, j
real*8                  :: y, Z(ndv,1) 
real*8                  :: f, er, Theta
real*8, dimension(ndv)  :: x, ub, lb, xStar, xTrasla 
real*8, dimension(3)    :: Phi, TH, a, w, b
real*8                  :: R(ndv,ndv), V(ndv,ndv-1) 

 ub(1:ndv) = 0.2d0
 lb(1:ndv) = -0.1d0
 x(:) = 0.d0
 open(15, file='variables.inp', status='old', action='read')
 do i = 1,ndv
 	read(15,*) x(i)
	 x(i) = x(i)*(ub(i)-lb(i))+lb(i)
 enddo
 close(15)

 y = 0.d0
 Theta = 0.2d0
 Phi = (/ 10000.d0, 5000.d0, 2500.d0 /)

 do i = 1,ndv
	xStar(i) = 0.1d0
	Xtrasla(i) = x(i) - xStar(i)
 enddo

 V(:,:) = 0.d0

 call rotmnd(ndv,V,Theta,R)

 call dgemm( 'N', 'N', ndv, 1, ndv, 1.0d0, R, ndv, xTrasla, ndv, 0.0d0, Z, ndv)

 f = 0.d0
 do i = 1,ndv 
 	f = f + (z(i,1)**2.d0 + 1.d0 - cos(10.d0*pi*z(i,1)))
 enddo
 TH(:) = 1.d0-0.0001d0*Phi(:)
 a(:) = TH(:)
 w(:) = 10.d0*pi*TH(:)
 b(:) = 0.5d0*pi*TH(:)

 er = 0.d0	
 do i = 1,ndv
     er = er + &
     & a(FidelityLevel)*(cos(w(FidelityLevel)*z(i,1)+b(FidelityLevel)+pi)**2.d0)
 enddo
	
 y = f + er

End Subroutine
