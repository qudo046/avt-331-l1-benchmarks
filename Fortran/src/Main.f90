Program MainL1
implicit none
integer, parameter                :: iCase = 15, iPlot = 125
integer                           :: FidelityLevel
real*8                            :: y
 character(len=50)                :: InfoCase

 namelist/CaseInfo/ InfoCase, FidelityLevel

 open(iCase, file='L1-input.nml', status='OLD')
   read(iCase,NML=CaseInfo)
 close(iCase)

 if(InfoCase(1:11).eq.'1DFORRESTER' .or. &
   &InfoCase(1:11).eq.'1dforrester')   call Forrester(FidelityLevel,y)
 if(InfoCase(1:6).eq.'1DALOS' .or. &
   &InfoCase(1:6).eq.'1dalos')         call Alos1(FidelityLevel,y)
 if(InfoCase(1:6).eq.'2DALOS' .or. &
   &InfoCase(1:6).eq.'2dalos')         call Alos2(FidelityLevel,y)
 if(InfoCase(1:6).eq.'3DALOS' .or. &
   &InfoCase(1:6).eq.'3dalos')         call Alos3(FidelityLevel,y)
 if(InfoCase(1:12).eq.'2DROSENBROCK' .or. &
   &InfoCase(1:12).eq.'2drosenbrock')  call Rosenbrock2(FidelityLevel,y)
 if(InfoCase(1:12).eq.'5DROSENBROCK' .or. &
   &InfoCase(1:12).eq.'5drosenbrock')  call Rosenbrock5(FidelityLevel,y)
 if(InfoCase(1:13).eq.'10DROSENBROCK' .or. &
   &InfoCase(1:13).eq.'10drosenbrock') call Rosenbrock10(FidelityLevel,y)
 if(InfoCase(1:11).eq.'2DRASTRIGIN' .or. &
   &InfoCase(1:11).eq.'2drastrigin')   call Rastrigin2(FidelityLevel,y)
 if(InfoCase(1:11).eq.'5DRASTRIGIN' .or. &
   &InfoCase(1:11).eq.'5drastrigin')   call Rastrigin5(FidelityLevel,y)
 if(InfoCase(1:12).eq.'10DRASTRIGIN' .or. &
   &InfoCase(1:12).eq.'10drastrigin')  call Rastrigin10(FidelityLevel,y)
 if(InfoCase(1:6).eq.'2DMASS' .or. &
   &InfoCase(1:6).eq.'2dmass')         call Mass(FidelityLevel,y)
 if(InfoCase(1:8).eq.'2DSPRING' .or. &
   &InfoCase(1:8).eq.'2dspring')       call Spring(FidelityLevel,y)
 if(InfoCase(1:12).eq.'4DSPRINGMASS' .or. &
   &InfoCase(1:12).eq.'4dspringmass')  call SpringMass(FidelityLevel,y)

 open(iPlot,file='Output.dat', status='unknown',action='write')
   write(iPlot,*) y
   write(*,*) y
 close(iPlot)
 
End Program
