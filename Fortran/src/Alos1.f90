Subroutine Alos1(FidelityLevel,y)
implicit none
integer, parameter     :: ndv = 1
integer                :: FidelityLevel, i
real*8                 :: y, f 
real*8, dimension(ndv) :: x, ub, lb 

 ub(1:ndv) = 1.d0
 lb(1:ndv) = 0.d0
 x(:) = 0.d0
 open(15, file='variables.inp', status='old', action='read')
 do i = 1,ndv
 	read(15,*) x(i)
	 x(i) = x(i)*(ub(i)-lb(i))+lb(i)
 enddo
 close(15)

 y = 0.d0	
 f = sin(30.d0*( (x(1)-0.9d0)**4.d0 ))*cos(2.d0*(x(1)-0.9d0)) + &
   & + (x(1)-0.9d0)/2.d0
 if(FidelityLevel.eq. 1) y = f
 if(FidelityLevel.eq. 2) y = (f - 1.d0 + x(1))/(1.d0+0.25d0*x(1))

End Subroutine
