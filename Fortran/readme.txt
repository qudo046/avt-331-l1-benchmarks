%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NATO-AVT-331 -- L1-Problems Fortran Tool_v1.0				%
% Developed by CNR-INM							%
% Released: 01/07/2021 							%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
This tool contains the implementation in fortran of the L1-Problems 
 
COMPILE WITH MAKEFILE:
---------------------
To compile the tool with the gfortan compiler: make gnu=1 
The executable is contained in the bin folder. 
Run the executable in the example folder: ../bin/gnuL1Problems 

To compile the tool with the intel compiler:   make intel=1 
The executable is contained in the bin folder. 
Run the executable in the example: ../bin/iL1Problems  

LIST OF INPUT:
-------------
In order to be run the executable needs two Input files: 

1 - L1-Input.nml -- a namelist which contains the information about the 
                    test case (InfoCase) and the fidelity level (FidelityLevel) 
                    to sample. 
  --->	For example: 
	InfoCase = ‘1DFORRESTER’ 
	FidelityLevel = 2 
	The second fidelity level of the Forrester function will be evaluated. 

2 - variables.inp -- contains the coordinates of the point in which the 
                     function will be analyzed. 
  --->	ALLERT: THE VARIABLES VALUE MUST BE WRITE IN A NORMALIZED DOMAIN 
        (BETWEEN 0 AND 1). THE VARIABLES DOMAIN ARE DENORMALIZED IN THE
        FORTRAN SUBROUTINES. 

OUTPUT:
------
In Output is presented the file ---> Output.dat which contains the value of the 
function evaluated in the point indicated in the file variables.inp. 

 
NAMELIST SUMMARY:
----------------
Name of  the L1 Problems for the tool 

InfoCase:  1DFORRESTER,       N fid. = 4, ndv=1,   x C [0,1]             

	   1DALOS,            N fid. = 2, ndv=1,   x C [0,1] 

           2DALOS,            N fid. = 2, ndv=2,   x C [0,1] 

           3DALOS,            N fid. = 2, ndv=3,   x C [0,1] 

           2DROSENBROCK,      N fid. = 3, ndv=2,   x C [-2,2] 

           5DROSENBROCK,      N fid. = 3, ndv=5,   x C [-2,2] 

           10DROSENBROCK,     N fid. = 3, ndv=10,  x C [-2,2] 

           2DRASTRIGIN,       N fid. = 3, ndv=2,   x C [-0.1, 0.2] 

           5DRASTRIGIN,       N fid. = 3, ndv=5,   x C [-0.1, 0.2] 

           10DRASTRIGIN,      N fid. = 3, ndv=10,  x C [-0.1, 0.2] 

           2DMASS,            N fid. = 2, ndv=2,   x C [1, 4],        Variables : x1=m1, x2=m2  (k1=k3=4, k2=2) 

           2DSPRING,          N fid. = 2, ndv=2,   x C [1, 4]         Variables : x1=k1, x2=k2  (m1=m2=1, k3=4) 

           4DSPRINGMASS,      N fid. = 2, ndv=4,   x C [1, 4]         Variables : x1=k1, x2=k2, x3=m1, x4=m2  

